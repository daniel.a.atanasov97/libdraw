#include "image.hpp"

#include <iostream>

draw::point rotate_around(draw::point const pivot, draw::point which, double const rad) {
  which.x -= pivot.x;
  which.y -= pivot.y;

  auto const cos = std::cos(rad);
  auto const sin = std::sin(rad);

  auto const x = which.x * cos - which.y * sin;
  auto const y = which.x * sin + which.y * cos;

  which.x = x + pivot.x;
  which.y = y + pivot.y;

  return which;
}

int main() {
  draw::image img1{ 500, 500, { 255, 255, 127 } };
  draw::image img2{ 5000, 5000, { 255, 255, 127 } };

  draw::point p1{ 100, 100 };
  draw::point p2{ 100, 400 };
  draw::point p3{ 400, 100 };
  draw::point p4{ 400, 400 };

  std::filesystem::path const out1 = "out_1";
  std::filesystem::path const out2 = "out_2";

  if (!exists(out1)) {
    create_directories(out1);
  }

  if (!exists(out2)) {
    create_directories(out2);
  }

  img1.set_blending_mode(draw::image::OneMinus);
  img1.fill_rect(50, 50, 450, 450, { 255, 255, 255 }, { 0, 0, 0 });

  for (auto id = 0; id < 90; id++) {
    img1.fill_circle(250, 250, 150, { 127, 255, 255, 0.5f }, { 255, 0, 0 });
    img1.draw_spline({ p1, p2, p3, p4 }, { 0, 255, 0, 0.1f });
    img1.draw_spline({ p1, p3, p2, p4 }, { 0, 0, 255, 0.1f });

    //img2.draw_spline({ p1 * 10, p2 * 10, p3 * 10, p4 * 10 }, { 0, 255, 0 });
    //img2.draw_spline({ p1 * 10, p3 * 10, p2 * 10, p4 * 10 }, { 0, 0, 255 });

    img1.save_tga(out1 / std::to_string(id));
    //img2.save_tga(out2 / std::to_string(id));

    p1 = rotate_around({ 250, 250 }, p1, 5);
    p2 = rotate_around({ 250, 250 }, p2, 5);
    p3 = rotate_around({ 250, 250 }, p3, 5);
    p4 = rotate_around({ 250, 250 }, p4, 5);

    std::cout << id << std::endl;
  }

  return 0;
}

